<?php
session_start();
$photo = $_SESSION['user']['avatar']
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../assets/css/shablon.css">
    <link type="text/css" rel="stylesheet" href="../../assets/css/bought_vocabulary_list.css">
</head>
<body>
<?php
require("shablon.php");
?>
<div class="box_bought_vocabulary_list">
    <div class="box_value">
        <div class="first_box">
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/travel.svg" width="30" height="30">
                </div>
                <div>
                    <a href="words.php">GEOGRAPHY</a><br>
                </div>

            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/geology.svg" width="30" height="30">
                </div>
                <a href="words_vocabular_geology.php">GEOLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/kangaroo.svg" width="30" height="30">
                </div>
                <a href="">ZOOLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/law.svg" width="30" height="30">
                </div>
                <a href="">LAW</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/chemistry (1).svg" width="30" height="30">
                </div>
                <a href="">CHEMISTRY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/ecology.svg" width="30" height="30">
                </div>
                <a href="">ENVIRONMENT</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/graduated.svg" width="30" height="30">
                </div>
                <a href="">EDUCATION</a><br>
            </div>
        </div>
    </div>

    <div class="box_value">
        <div class="second_box">
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/gold.svg" width="30" height="30">
                </div>
                <a href="">ECONIMICS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/chevron.svg" width="30" height="30">
                </div>
                <a href="">MILITARY AFFAIRS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/footprint.svg" width="30" height="30">
                </div>
                <a href="">ARCHEOLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/agriculture.svg" width="30" height="30">
                </div>
                <a href="">AGRICULTURE</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/moon.svg" width="30" height="30">
                </div>
                <a href="">METEOROLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/totem.svg" width="30" height="30">
                </div>
                <a href="">ANTHROPOLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/leadership.svg" width="30" height="30">
                </div>
                <a href="">CHARACTER</a><br>
            </div>
        </div>
    </div>
    <div class="box_value">
        <div class="third_box">
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/sociology.svg" width="30" height="30">
                </div>
                <a href="">SOCIOLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/dna.svg" width="30" height="30">
                </div>
                <a href="">BIOLOGY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/calculate.svg" width="30" height="30">
                </div>
                <a href="">MATHS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/planet.svg" width="30" height="30">
                </div>
                <a href="">ASTRONOMY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/science.svg" width="30" height="30">
                </div>
                <a href="">PHYSICS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/medicine.svg" width="30" height="30">
                </div>
                <a href="">MEDICINE</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/paint.svg" width="30" height="30">
                </div>
                <a href="">ART</a><br>
            </div>
        </div>
    </div>
    <div class="box_value">
        <div class="fourth_box">
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/electric-guitar.svg" width="30" height="30">
                </div>
                <a href="">MUSIC</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/languages.svg" width="30" height="30">
                </div>
                <a href="">LINGUISTICS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/political.svg" width="30" height="30">
                </div>
                <a href="">POLITICS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/leaves.svg" width="30" height="30">
                </div>
                <a href="">BOTANICS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/mountain.svg" width="30" height="30">
                </div>
                <a href="">NATURE</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/yin-yang-symbol.svg" width="30" height="30">
                </div>
                <a href="">RELIGION</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/shield.svg" width="30" height="30">
                </div>
                <a href="">QUALITY</a><br>
            </div>
        </div>
    </div>
    <div class="box_value">
        <div class="fifth_box">
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/cabbage.svg" width="30" height="30">
                </div>
                <a href="">FOOD</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/360.svg" width="30" height="30">
                </div>
                <a href="">REALITY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/heartbeat.svg" width="30" height="30">
                </div>
                <a href="">INNER FEELINGS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/behaviour.svg" width="30" height="30">
                </div>
                <a href="">BEHAVIOUR</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/communication.svg" width="30" height="30">
                </div>
                <a href="">LINGUISTICS</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/house.svg" width="30" height="30">
                </div>
                <a href="">PROPERTY</a><br>
            </div>
            <div class="otstup">
                <div class="icon_text1">
                    <img src="../../icon/time.svg" width="30" height="30">
                </div>
                <a href="">CONDITION</a><br>
            </div>
        </div>
    </div>
</div>
</body>
</html>


