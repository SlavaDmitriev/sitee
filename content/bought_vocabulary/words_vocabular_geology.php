<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../assets/css/shablon.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/words.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/words_vocabylary_geology.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/color_geology.css">
</head>
<body>
<?php
require("shablon.php");
?>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>vein</h1>[vein] n.</p>
            <br> the frame of a leaf or an insect's wing </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/1.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>pit</h1>[ pit] n.</p>
            <br>(hole); a large hole in the ground, or a slightly low area in any surface
            <br>
            <br>ex. The coal-mining industry wants new pits to be opened. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/2.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>borehole</h1>['bɔɪhəul] n.</p>
            <br>a deep hole made in the ground when looking for oil, gas, or water
            <br>
            <br>ex. They obtained information about the rock by drilling boreholes. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/3.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>quartz</h1>[kwɔ:ts] n.</p>
            <br> a hard, transparent mineral substance, used in making electronic equipment and accurate watches and clocks
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/4.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>marble</h1>['mɑɪbl] n.</p>
            <br>a type of very hard rock that has a pattern of lines going through it, feels cold, and can be polished to become smooth and shiny
            <br>
            <br>a marble floor/ statue </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/5.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>gem</h1>[dʒem] n.</p>
            <br>(jewel, precious stone);
            <br>
            <br> </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/6.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>fieldstone</h1>['fiɪldstəun] n.</p>
            <br>stone used in its natural form
            <br>
            <br>ex. The fieldstone fireplace and comfortable furnishings help everyone feel welcome relaxed. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/7.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>emerald</h1>['emərəld] n.</p>
            <br>a transparent, bright green, valuable stone that is often used in jewellery
            <br>
            <br>a ring with a large emerald, an emerald necklace </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/8.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>granite</h1>['grænit] n.</p>
            <br>a very hard, grey, pink, or black rock, used for building
            <br> </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/9.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1> lead</h1>[li:d] n.</p>
            <br>a chemical element that is a very heavy, soft, dark grey, poisonous metal, used especially in the past on roofs and for pipes and also for protection against radiation <br>
            <br>lead pipes </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/10.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>limestone</h1>['laimstəun] n.</p>
            <br>a white or light grey rock that is used as a building material and in the making of cement
            <br>
            <br> </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/11.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>lava</h1>['lɑ:və] n.</p>
            <br>hot liquid rock that comes out of the earth through a volcano, or the solid rock formed when it cools
            <br> </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/12.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>ruby</h1>['ru:bi] n.</p>
            <br>a transparent, dark red precious stone, often used in jewellery<br>
            <br>ex. The dress is available in several colours, including chocolate and ruby red </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/13.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>bonanza</h1>[bə'nænzə] n.</p>

            <br>ex. Winning the lottery was a bonanza for the Browns.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/14.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>mineral</h1>['minərəl] n.</p>
            <br>a valuable or useful chemical substance that is formed naturally in the ground
            <br>
            <br>ex. a mineral supplement</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/15.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>ore</h1>[ɔːr] n.</p>
            <br>rock or soil from which metal can be obtained
            <br> </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/16.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>sediment</h1>['sedimənt] n.</p>

            <br>ex. There was a brown sediment in the bottom of the bottle. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/17.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>fossil</h1>['fɔsl] n.</p>
            <br>the shape of a bone, a shell, or a plant or animal that has been preserved in rock for a very long period
            </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/18.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1> petrify </h1>['petrifai] v.</p>
            <br> to frighten someone a lot, especially so that they are unable to move or speak
            <br>
            <br>ex. I was totally petrified with fear.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/19.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>geology</h1>[ʤi'ɔləʤi] n.</p>
            <br>the study of the rocks and similar substances that make up the earth's surface
           </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/20.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1> aluminum </h1>[ə'ljuɪminəm] n.</p>
            <br> a chemical element that is a light, silver-coloured metal, used especially for making cooking equipment and aircraft parts
            <br>
            <br>ex. We take all our aluminium cans for recycling</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/21.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>core</h1>[kɔ:] n.</p>
            <br>the basic and most important part of something
            <br>
            <br>ex. The earth's core is a hot, molten mix of iron and nickel. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/22.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>crater</h1>['kreitə] n.</p>
            <br>the round hole at the top of a volcano, or a hole in the ground similar to this
            <br>
            <br>a bomb crater</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/23.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>diamond</h1>['daiəmənd] n.</p>

            <br>ex. He had worked in the diamond mines of South Africa.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/24.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>glacial</h1>[gleisjəl] n.</p>
            <br> glacial drift, glacial epoch
            <br>
            <br>ex. I have never visited the place with glacial temperatures. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/25.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>iceberg</h1>[aisbəɪg ] n.</p>
            <br>a very large mass of ice that floats in the sea
            <br>
            <br>
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/26.jpg" width="300" height="300"></div>
</div>

<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>plate</h1>[pleit] n.</p>
            <br> a flat piece of something that is hard, does not bend, and is usually used to cover something
            <br>
            <br>ex. Large, metal plates covered the trench in the roadway. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/27.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>tremor</h1>['tremə] n.</p>
            <br>a slight earthquake
            <br>
            <br>ex. The tremor was felt as far as 200 miles away. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/28.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>earthquake</h1>[ə:θkweik] n.</p>
            <br>a sudden violent movement of the earth's surface, sometimes causing great damage
            <br>
            <br>ex. This was the first successful earthquake prediction in history.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/29.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>seismic</h1>['səizmik] adj.</p>
            <br> seismic activity/ waves
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/30.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>seismology</h1>[siz 'mɔləʤi] n.</p>
            <br>the scientific study of the sudden, violent movements of the earth connected with earthquakes
          </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/31.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>magnitude</h1>['mægnitju:d] n.</p>
            <br>the magnitude of an earthquake is a measure of how strong or violent it is
           </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/32.png" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1> cataclysm</h1>['kætəklizəm] n.</p>
            <br>an event that causes a lot of destruction, or a sudden, violent change
            <br>
            <br>
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/33.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>stratum</h1>[‘streitəm] n.</p>
            <br>a layer of rock, soil, or similar material
           </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/34.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>mantle</h1>[‘mæntl] n.</p>
            <br>the part of the earth that surrounds the central core
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/35.png" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>lithosphere</h1>['liθəisfiə] n.</p>
            </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/36.png" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>layer</h1>['leiə] n.</p>
            <br>a level of material, such as a type of rock or gas, that is different from the material above or below it, or a thin sheet of a substance
            <br>
            <br>the ozone layer </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/37.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>crust</h1>[krast] n.</p>
            <br>(shell);
            <br>
            <br>ex. The crust of the pie was delicious. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/38.png" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>fault</h1>[fɔ:lt] n.</p>
            <br>(fault plane; fault zone);
            <br>a crack in the earth's surface where the rock has divided into two parts that move against each other
            <br>
            <br>ex. Surveyors say the fault line is capable of generating a major earthquake once in a hundred years. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/39.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1> magma </h1>['mægmə] n.</p>
            <br>hot liquid rock found just below the surface of the earth
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/40.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>squirt</h1>[skwə:t] v.</p>
            <br>(spurt, to force a liquid) to flow out through a narrow opening in a fast stream
            <br>
            <br>ex. He squirted some tomato sauce on his burger.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/41.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>erupt</h1>[i'rʌpt] adj.</p>
            <br>(eruption; explode, burst out);
            <br>
            <br>ex. We feared that the volcano would erupt again.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/42.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>outburst</h1>['əutbə:st] adj.</p>
            <br>(surge, explosion);
            <br>
            <br>ex. Her comments provoked an outburst of anger from the boss. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/43.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p>
            <h1>volcanic</h1>[vol’kænik] adj.</p>
            <br>(volcanic island, volcanic ash, volcanic dust);
            <br>
            <br>ex. They claimed to get on well despite their volcanic rows.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geology/44.jpg" width="300" height="300"></div>
</div>

</body>
</html>