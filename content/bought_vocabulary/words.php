<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../assets/css/shablon.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/words.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/color_geography.css">

</head>


<body>
<?php
require("shablon.php");
?>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>insular</h1>['insjulə] adj.</p>
        <br><br>ex. The people who live on the mountain have insular personalities because they are not used to being around other members of society.
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/1.jpg" width="300" height="300"></div>
</div>

<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
                    <p><h1>island</h1>['ailənd] n.</p>
            ex. Last we visited coral island (continental island).
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/2.jpg" width="300" height="300"></div></div>
</div>

<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>peninsula</h1> [pi'ninsjulə] n.</p>
        <br><br>ex. We took a trip to the peninsula which was surrounded by water on all three sides.
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/3.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>islet</h1>['ailit] n.</p>
        <br>ex. There are performed some photos of an islet in the San Blas archipelago.
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/4.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>marine</h1>[mə'riɪn] adj.</p>
        <br>(marine, oceanic); <br><br>ex. Human beings are natural enemies of marine mammals</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/5.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>maritime</h1>[mæritaim] adj.</p>
        <br>(marine, oceanic);<br><br>ex. Amalfi and Venice were important maritime powers. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/6.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>moist</h1>[mɔɪst] adj.</p>
        <br>(damp, humid);<br><br>ex. Keep the soil in the pot moist, but not too wet. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/7.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>ledge</h1>[ledʒ] n.</p>
        <br> a narrow, flat area like a shelf that sticks out from a building, cliff, or other vertical surface <br><br>ex. The space craft rests on a ledge, which is pushed along the track using the power of electro magnets. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/8.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>oasis</h1>[əu'eisis ] n.
        </p>
        <br> a place in a desert where there is water and therefore plants and trees and sometimes a village or town <br><br>ex. The travellers were saved when they finally found an oasis. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/9.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>tide</h1>[taid] n.</p>
        <br> the rise and fall of the sea that happens twice every day <br><br>ex. Most people are completely clueless about tide directions and weather conditions. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/10.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>ebb</h1>[eb] v.</p>
        <br> when the sea or tide ebbs, it moves away from the coast and falls to a lower level <br><br>ex. The water washed up on the shore, then slowly ebbed away. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/11.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>continent</h1>[k'ɔntinənt ] n.</p>
        <br>ex. the North American continent </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/12.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>terrestrial</h1>[ti'restriəl ] adj.</p>
        <br> (terrestrial heat, terrestrial magnetism);<br><br>ex. Terrestrial television channels are broadcast from stations on the ground and do not use satellites. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/13.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>outskirts</h1>[aut.skə:ts] n.</p>
        <br> (surrounding); <br><br>ex. The factory is on the outskirts of the city. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/14.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>region</h1>[ 'ri:ʤən] n.</p>
        <br> zone, area, field, domain </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/15.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>endemic</h1>[ en’demik ] adj.</p>
        <br> (native);<br><br>ex.  This disease is endemic to the southerners, and will not spread in the cold north. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/16.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>cosmopolitan</h1>[,kɔzmə'pɔlitən] adj.</p>
        <br>(global);<br><br>ex. The farmer was unused to the cosmopolitan ways of life in a large city. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/17.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>subterranean</h1>[sʌbtə'reiniən] adj. </p>
        <br>subterranean passages, a subterranean river</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/18.jpg" width="300" height="300"></div>
</div>

<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>cavern</h1>['kævən] n.</p>
        <br> a large cave </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/19.jpg" width="300" height="300"></div>
</div>

<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>flaw</h1>[flɔː] n. </p>
        <br> a fault, mistake, or weakness, especially one that happens while something is being planned or made, or that causes something not to be perfect <br><br>ex. The flaw in your theory is that you didn't account for gravity. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/20.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>cleft</h1>[kleft] n.</p>
        <br> (crevice);<br>an opening or crack, especially in a rock or the ground <br><br>ex. Eagles often nest in a cleft in the rocks. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/21.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>gap</h1>[gæp] n.</p>
        <br>(opening);<br><br>ex. Generation gap lies between parents and children. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/22.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>zone</h1>[ zəun] n. </p>
        <br>(region, area);<br> time zone </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/23.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>equator</h1>[i'kweitə] n.</p>
        <br> It is warmer near the equator. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/24.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>longitude</h1>['lɔndʒitju:d] n.</p>
        <br> the di the North Pole and the South Pole, measured in degrees </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/25.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>altitude</h1>['æɪtitju:d] n.</p>
        <br> height above sea level </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/26.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>latitude</h1>[ 'lætitju:d] n.</p>
        <br> the position north or south of the equator measured from 0° to 90° </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/27.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>meridian</h1>[mə'ridiən] n.</p>
        <br><br>ex. The prime meridian of longitude is in Greenwich, London.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/28.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>subsidiary</h1>[səb'sidjəri] n.</p>
        <br>(branch);<br> used to refer to something less important than something else with which it is connected <br><br>ex. He followed the subsidiary to trace its origin </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/29.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>Antarctic</h1>[ænt'a:ktik] adj.</p>
        <br><br>ex. The protection of the Antarctic from commercial exploitation is an important goal of environmentalists. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/30.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>Arctic</h1>['aɪktik] adj.</p>
        <br> the very cold area around the North Pole <br><br>ex. Polar bears live in the Arctic. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/31.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>coastline</h1>['koust.laɪn] n.</p>
        <br>Within a few years, the coastline was transformed in a 700 billion-lire tourist paradise. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/32.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>hemisphere</h1>['hemisfiə] n.</p>
        <br>the northern/southern hemisphere
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/33.jpg" width="300" height="300"></div>
</div>

<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>contour</h1>['kɔntuə] n.</p>
        <br>(outline, profile);<br><br>ex. The artist accentuated the model's contours. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/34.png" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>geography</h1>[dʒi'a:grəfi] n.</p>
        <br><br>ex. It's impossible to figure out the geography of this hospital. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/35.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>horizon</h1>[hə'raɪzn] n.</p>
        <br><br>ex. The moon rose slowly above the horizon. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/36.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>lowland</h1>[`ləulənd, -'lænd ] n.</p>
        <br>flat land that is at, or not much higher than, sea level<br><br>ex. From the lowlands of the south to the rugged peaks in the north, this area has something for everyone. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/37.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>plain</h1>[plein] n.</p>
        <br><br>ex. He prefers plain food - nothing too fancy. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/38.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>strait</h1>[streit] n.</p>
        <br>the Straits of Gibraltar
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/39.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>channel</h1>['tʃænl] n.</p>
        <br>the area of sea that separates England from France </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/40.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>valley</h1>['væli]</p>
        <br>an area of low land between hills or mountains, often with a river running through it </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/41.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>volcano</h1>[vɔl'keinəu] n.</p>
        <br>an extinct/dormant volcano, an active volcano </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/42.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>plateau</h1>[plæ'təu] n.</p>
        <br>a large flat area of land that is high above sea level </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/43.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>basin</h1>['beisn] n.</p>
        <br>the basin of a river or body of water is the land that surrounds it and the streams that flow into it <br><br>ex. The Amazon basin covers a vast amount of land. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/44.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>navigation</h1>[nævi'geiʃən] n.</p>
        <br>the act or science of finding a way from one place to another the navigation aid </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/45.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>ranges</h1>[reinʤiz] n.</p>
        <br>a set of similar, things a mountain range </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/46.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>salinity</h1>[sə'liniti] n.</p>
        <br>the fact of containing salt of the amount of salt contained in something <br><br>ex. You should test the salinity of the water.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/47.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>sediment</h1>['sedimənt] n.</p>
        <br><br>ex. There was a brown sediment in the bottom of the bottle.
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/48.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>elevation</h1>[eli'veiʃən] n.</p>
        <br><br>ex. There was a brown sediment in the bottom of the bottle. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/49.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>formation</h1>[fɔ:'meiʃən] n. </p>
        <br>a rock formationm, cloud formations</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/50.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>geothermal</h1>[dʒi'ɔθəməl] adj.</p>
        <br>of or connected with the heat inside the earth<br>a geothermal power station </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/51.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>terrain</h1>['terein] n.</p>
        <br>ground;<br><br>ex. The car handles particularly well on rough terrain. </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/52.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>topography</h1>[tə'pa:grəfi] n.</p>
        <br>the natural features of land <br><br>ex. Volcanoes have sculpted the topography of the island.</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/53.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>tropical</h1>['trɔpikl] adj.</p>
        <br>a tropical island/region/climate </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/54.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
        <p><h1>tropics</h1>['trɔpiks] n.</p>
        <br>the hottest area of the earth</div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/55.jpg" width="300" height="300"></div>
</div>
<div class="words">
    <div class="words_text_bord color">
        <div class="words_text">
            <p><h1>temperate</h1>['tempərit] adj.</p>
            <br>a temperate climate, temperate latitudes
        </div>
    </div>
    <div class="words_image"><img src="../../assets/img/geography/56.jpg" width="300" height="300"></div>
</div>
