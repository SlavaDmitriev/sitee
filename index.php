<?php
session_start();
if ($_SESSION['user']) {
    unset($_SESSION['user']);
}
require ("test.php");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="courses.css">
    <link rel="stylesheet" type="text/css" href="assets/css/phot.css">
    <title>Document</title>

    <link rel="stylesheet" href="js/js.js">
    <link rel="script" href="logo.js">
</head>
<style>
    html{
        scroll-behavior: smooth;
    }
</style>



<div class="full">
<header>
    <div class="nav_b">
        <div class="topnav" id="myTopnav">
            <div id="logo"><img src="../../icon/vk.svg" width="30" height="30"></div>
            <div class="bar">
                <div class="nu"><a href="#hom1e"></a></div>
                <a href="#home">Home</a>
                <a href="#news">News</a>
                <a href="#contact">Contact</a>
                <div class="dropdown">
<!--                    <button class="dropbtn">Dropdown-->
<!--                        <i class="fa fa-caret-down"></i>-->
<!--                    </button>-->
                    <a class="dropbtn">Dropdown
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-content">
                        <a href="#">Link 1</a>
                        <a href="#">Link 2</a>
                        <a href="#">Link 3</a>
                    </div>
                </div>
                <div class="dropdown">
<!--                    <button class="dropbtn">Демо-->
<!--                    </button>-->
                    <a class="dropbtn">Демо
                    </a>
                    <div class="dropdown-content">
                        <a href="#">Link 1</a>
                        <a href="#">Link 2</a>
                        <a href="#">Link 3</a>
                    </div>
                </div>
                <a href="#about">About</a>
                <a href="inde.php">Login</a>

            </div>
            <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
        </div>
    </div>
</header>
</div>

<div class="part">
    <header class="header">
<!--        <div class="nav_b">-->
<!--            <div class="topnav" id="myTopnav">-->
<!--                <div id="logo"><img src="../../icon/vk.svg" width="30" height="30"></div>-->
<!--                <div class="bar">-->
<!--                    <div class="nu"><a href="#hom1e"></a></div>-->
<!--                    <a href="#home">Home</a>-->
<!--                    <a href="#news">News</a>-->
<!--                    <a href="#contact">Contact</a>-->
<!--                    <div class="dropdown">-->
                        <!--                    <button class="dropbtn">Dropdown-->
                        <!--                        <i class="fa fa-caret-down"></i>-->
                        <!--                    </button>-->
<!--                        <a class="dropbtn">Dropdown-->
<!--                            <i class="fa fa-caret-down"></i>-->
<!--                        </a>-->
<!--                        <div class="dropdown-content">-->
<!--                            <a href="#">Link 1</a>-->
<!--                            <a href="#">Link 2</a>-->
<!--                            <a href="#">Link 3</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="dropdown">-->
<!--                        <a class="dropbtn">Демо-->
<!--                        </a>-->
<!--                        <div class="dropdown-content">-->
<!--                            <a href="#">Link 1</a>-->
<!--                            <a href="#">Link 2</a>-->
<!--                            <a href="#">Link 3</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <a href="#about">About</a>-->
<!--                    <a href="inde.php">Login</a>-->
<!---->
<!--                </div>-->
<!--                <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>-->
<!--            </div>-->
<!--        </div>-->
        <a href="" class="logo">CSS Nav</a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
            <li><a href="#work">Our Work</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#careers">Careers</a></li>
            <li><a href="#contact">Contact</a></li>

            <input type="checkbox" id="drop" />
            <li>
                <label for="drop-1" class="toggle">Service +</label>
                <input type="checkbox" id="drop-1" />
                <ul>
                    <li><a href="#">Service 1</a></li>
                    <li><a href="#">Service 2</a></li>
                    <li><a href="#">Service 3</a></li>
                </ul>
            </li>
        </ul>


    </header>
</div>








<div class="phot"></div>
<div class="tex">
    <h1>5 ШАГОВ К TOEFL</h1>
</div>
<div class="text">
    <h1>Lorem ipsum dolor s Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure magni officia possimus sed unde! Culpa dicta dolorem, earum esse, est facere nam nisi officia pariatur possimus recusandae vel velit. Accusamus beatae commodi cum dignissimos doloribus ducimus excepturi incidunt magnam molestias mollitia nam, necessitatibus quaerat quam quod ut. Enim, neque temporibus. it amet, consectetur adipisicing elit. A, aperiam beatae blanditiis consectetur cupiditate deleniti dignissimos doloribus dolorum eius eligendi est et fugit iste minima minus mollitia, nisi nulla perferendis quia quibusdam, quidem quis quos recusandae rerum sapiente vero voluptatum! Accusantium fugiat nihil numquam odit optio reiciendis similique sit ut.</h1>
</div>


<!--<div id="news" class="for">-->
<div class="for">
    <h2 style="text-align: center">Frequently Asked Questions</h2>
    <br />
    <div>
        <details class="for1">
            <summary class="for2">What is Lorem Ipsum?</summary>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </details>
    </div>
    <div>
        <details class="for1">
            <summary class="for2">Where does it come from?</summary>
            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
        </details>
    </div>
    <div>
        <details class="for1">
            <summary class="for2">Why do we use it?</summary>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
        </details>
    </div>
</div>



<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>


<script src="js/js.js"></script>
</body>
</html>


