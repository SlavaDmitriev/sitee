<?php
    session_start();
    if ($_SESSION['user']) {
        header('Location: login_person.php');
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация и регистрация</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

    <!-- Форма регистрации -->

    <form action="vendor/signup.php" method="post" enctype="multipart/form-data">
        <label>ФИО</label>
        <input type="text" name="full_name" placeholder="Введите свое полное имя" required>
        <label>Логин</label>
        <input type="text" name="login" placeholder="Введите свой логин" required>
        <label>Почта</label>
        <input type="email" name="mail" placeholder="Введите адрес своей почты" required>
        <label>Изображение профиля</label>
        <input type="file" name="avatar">
        <label>Пароль</label>
        <input id="password" type="password"  name="password" placeholder="Введите пароль" required>
        <label>Подтверждение пароля</label>
        <input id="password" type="password" name="check_password" placeholder="Введите пароль" required>
        <button type="submit">Зарегистрироваться</button>
        <p>
            У вас уже есть аккаунт? - <a href="inde.php">авторизируйтесь</a>!
        </p>
        <?php
            if ($_SESSION['message']) {
                echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
            }
            unset($_SESSION['message']);
        ?>
    </form>

</body>
</html>